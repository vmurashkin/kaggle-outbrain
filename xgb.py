import sys
import mmh3
import numpy as np
import xgboost as xgb
from array import array
from itertools import ifilter
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from splitdata import parse_rows
from splitdata import check_split
    
MISSING_VALUE = -1.


def load_data(lines, mod=1, train_buckets=None, val_buckets=None, display_mod=1, display_buckets=None, buffer_size=100000000):

    display_buckets = display_buckets or {0}
    _chech_dispaly = lambda row: ((mmh3.hash('%s_asv342afse9sd01' % row[0]) & 0x7fffffff) % display_mod in display_buckets) 


    _check_split_train = lambda row: check_split(row, mod, train_buckets or {0}) and _chech_dispaly(row)
    _check_split_val = lambda row: check_split(row, mod, val_buckets or {0}) and _chech_dispaly(row)

    X_train, y_train, X_val, y_val = [], [], [], []

    def _handle_rows(buf, X, y, _check_fn):
        for display_id, ad_id, uuid_hash, clicked, x in ifilter(_check_fn, parse_rows(buf)):
            X.append(array('f', [MISSING_VALUE if e < 0. else e for e in x]))
            y.append(clicked)

    buf = []
    while True:
        del buf[:]
        try: buf.extend(lines.next() for _ in xrange(buffer_size))
        except StopIteration: pass
        if not buf: break
        _handle_rows(buf, X_train, y_train, _check_split_train)
        if val_buckets:
            _handle_rows(buf, X_val, y_val, _check_split_val)

    X_val, y_val = np.array(X_val), np.array(y_val)
    X_train, y_train = np.array(X_train), np.array(y_train)

    d_val = xgb.DMatrix(X_val, y_val, MISSING_VALUE) if val_buckets else None
    d_train = xgb.DMatrix(X_train, y_train, MISSING_VALUE)

    pos_num = y_train.sum()
    neg_num = len(y_train) - pos_num

    pos_weight = 1. * neg_num / (pos_num + 1e-32)

    return pos_weight, d_train, d_val 


def predict(lines, booster, buffer_size=2500000):
    buf = []
    while True:
        del buf[:]
        try: buf.extend(lines.next() for _ in xrange(buffer_size))
        except StopIteration: pass
        if not buf: break
        d_predict = load_data(iter(buf))[1]
        for p in booster.predict(d_predict):
            yield p


if __name__ == '__main__':

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--mod', type=int, default=7)
    parser.add_argument('--train_buckets', nargs='+', type=int, default=[2, 5]) 
    parser.add_argument('--val_buckets', nargs='+', type=int, default=[3]) 
    parser.add_argument('--display_mod', type=int, default=11) 
    parser.add_argument('--display_buckets', default=[0, 1, 2, 3, 4, 5]) 
    parser.add_argument('--random_state', type=int, default=12345)
    parser.add_argument('--model', default='xgb.model')
    parser.add_argument('--load_model', action='store_true')
    parser.add_argument('--predict')

    args = parser.parse_args()
    print >> sys.stderr, args

    if args.load_model:
        booster = xgb.Booster(model_file=args.model)
    else:
        pos_weight, d_train, d_val = load_data(sys.stdin,
                                               args.mod,
                                               set(args.train_buckets),
                                               set(args.val_buckets),
                                               args.display_mod, 
                                               set(args.display_buckets)) 

        print >> sys.stderr, 'train:%d, val:%d, pos_weight:%.3f' % (d_train.num_row(), d_val.num_row(), pos_weight)

        params = { 
          'nthread': 32,
          'objective': 'binary:logistic',
          'eval_metric': 'logloss',
          'learning_rate': .25,
          'max_depth': 3,
          'colsample_bytree': .25,
          'subsample': .75,
          'missing': MISSING_VALUE,
          'min_child_weight': 4,
          'scale_pos_weight': pos_weight,
          'seed': args.random_state}

        evals = [(d_train, 'train'), (d_val, 'val')]

        booster = xgb.train(params,
                            d_train,
                            early_stopping_rounds=100,
                            num_boost_round=25000,
                            evals=evals,
                            verbose_eval=20)

        booster.save_model(args.model)

    if not args.predict:
        exit(0)

    with open(args.predict) as src:
        for e in predict(src, booster):
            print e
