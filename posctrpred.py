import sys
import mmh3
from operator import itemgetter
from itertools import groupby


posctr = [0.233611849655
, 0.226578269039
, 0.207699499058
, 0.185316314774
, 0.154366929654
, 0.138317603296
, 0.114710654537
, 0.107826822909
, 0.0902247763758
, 0.0735632750065
, 0.0735663696596
, 0.0725449031957]


if __name__ == '__main__':

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--clicks')

    args = parser.parse_args()

    clicks = dict()

    with open(args.clicks) as src:
        src.next() # skip header
        lines = iter(map(int, l.rstrip().split(',')) for l in src)
        for display_id, v in groupby(lines, itemgetter(0)):
            clicks.update(((display_id, ad_id), posctr[i]) for i, (_, ad_id, clicked) in enumerate(v)) 

    lines = iter(map(int, l.rstrip().split(',')) for l in sys.stdin)
    for display_id, v in groupby(lines, itemgetter(0)):
        for _, ad_id, clicked in v:
            print clicks[(display_id, ad_id)]
