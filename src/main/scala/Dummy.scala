import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.rdd.RDD


object Dummy {

  case class Click(display_id: String, ad_id: String, clicked: Integer)
  case class PageView(uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String, traffic_source: Integer)
  case class Event(display_id: String, uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String)
  case class DocCategory(document_id: String, category_id: String, confidence_level: Double)
  case class DocEntity(document_id: String, entity_id: String, confidence_level: Double)
  case class DocMeta(document_id: String, source_id: String, publisher_id: String, publish_time: String)
  case class DocTopic(document_id: String, topic_id: String, confidence_level: Double)
  case class PromotedContent(ad_id: String, document_id: String, campaign_id: String, advertiser_id: String)


  def main(args: Array[String]) {

    var path_clicks_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train_parquet"
    var path_clicks_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test_parquet"
    var path_events_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_parquet"
    var path_page_views_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views_parquet"
    var path_doc_categories_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories_parquet"
    var path_doc_entities_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities_parquet"
    var path_doc_meta_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta_parquet"
    var path_doc_topics_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics_parquet"
    var path_promoted_content_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content_parquet"

    var path_ad_doc_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/ad_doc_ctr_parquet"
    var path_events_traffic_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_traffic_parquet"

    var path_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/train_parquet"
    var path_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test_parquet"

    val conf = new SparkConf().setAppName("Dummy App")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    sqlContext.read.parquet(path_test_parquet)
      .repartition(1)
      .write.format("com.databricks.spark.csv")
      .save("file:///home/vmurashkin/kaggle/outbrain/test")

    // sqlContext.read.parquet(path_train_parquet)
    //   .repartition(1)
    //   .write.format("com.databricks.spark.csv")
    //   .save("file:///home/vmurashkin/kaggle/outbrain/train")

    sc.stop()
  }
}
