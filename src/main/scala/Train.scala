import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import scala.util.hashing.MurmurHash3
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.rdd.RDD


object Train {

  case class Click(display_id: String, ad_id: String, clicked: Integer)
  case class PageView(uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String, traffic_source: Integer)
  case class Event(display_id: String, uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String)
  case class DocCategory(document_id: String, category_id: String, confidence_level: Double)
  case class DocEntity(document_id: String, entity_id: String, confidence_level: Double)
  case class DocMeta(document_id: String, source_id: String, publisher_id: String, publish_time: String)
  case class DocTopic(document_id: String, topic_id: String, confidence_level: Double)
  case class PromotedContent(ad_id: String, document_id: String, campaign_id: String, advertiser_id: String)


  def main(args: Array[String]) {

    var path_continent_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/continent_parquet"
    var path_clicks_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train_parquet"
    var path_clicks_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test_parquet"
    var path_events_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_parquet"
    var path_page_views_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views_parquet"
    var path_doc_categories_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories_parquet"
    var path_doc_entities_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities_parquet"
    var path_doc_meta_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta_parquet"
    var path_doc_topics_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics_parquet"
    var path_promoted_content_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content_parquet"

    var path_events_traffic_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_traffic_parquet"

    var path_user_seen_ad_ts = "hdfs:///user/vmurashkin/kaggle/outbrain/user_seen_ad_ts"
    var path_user_seen_campaign_ts = "hdfs:///user/vmurashkin/kaggle/outbrain/user_seen_campaign_ts"
    var path_user_click_ad_ts = "hdfs:///user/vmurashkin/kaggle/outbrain/user_click_ad_ts"
    var path_user_click_campaign_ts = "hdfs:///user/vmurashkin/kaggle/outbrain/user_click_campaign_ts"

    var path_user_profile = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_svd_parquet"


    // var path_ad_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/ad_ctr_parquet"
    // var path_doc_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/doc_ctr_parquet"
    // var path_platform_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/platform_ctr_parquet"
    // var path_traffic_source_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/traffic_source_ctr_parquet"
    // var path_geo_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/geo_ctr_parquet"
    // var path_dow_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/dow_ctr_parquet"
    // var path_category_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/category_ctr_parquet"
    // var path_continent_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/continent_ctr_parquet"
    // var path_country_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/country_ctr_parquet"
    // var path_state_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/state_ctr_parquet"
    // var path_dma_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/dma_ctr_parquet"
    // var path_publisher_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/publisher_ctr_parquet"
    // var path_source_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/source_ctr_parquet"
    // var path_campaign_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/campaign_ctr_parquet"
    // var path_advertiser_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/advertiser_ctr_parquet"

    // var path_ad_center = "hdfs:///user/vmurashkin/kaggle/outbrain/67/ad_center_parquet"
    // var path_campaign_center = "hdfs:///user/vmurashkin/kaggle/outbrain/67/campaign_center_parquet"

    // var path_clicks_parquet = path_clicks_train_parquet
    // var path_result_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/train_parquet"


    var path_ad_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/ad_ctr_parquet"
    var path_doc_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/doc_ctr_parquet"
    var path_platform_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/platform_ctr_parquet"
    var path_traffic_source_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/traffic_source_ctr_parquet"
    var path_geo_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/geo_ctr_parquet"
    var path_dow_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/dow_ctr_parquet"
    var path_category_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/category_ctr_parquet"
    var path_continent_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/continent_ctr_parquet"
    var path_country_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/country_ctr_parquet"
    var path_state_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/state_ctr_parquet"
    var path_dma_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/dma_ctr_parquet"
    var path_publisher_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/publisher_ctr_parquet"
    var path_source_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/source_ctr_parquet"
    var path_campaign_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/campaign_ctr_parquet"
    var path_advertiser_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/advertiser_ctr_parquet"

    var path_ad_center = "hdfs:///user/vmurashkin/kaggle/outbrain/67/test/ad_center_parquet"
    var path_campaign_center = "hdfs:///user/vmurashkin/kaggle/outbrain/67/test/campaign_center_parquet"

    var path_clicks_parquet = path_clicks_test_parquet
    var path_result_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test_parquet"


    def hash_fun(arg: String): Int = MurmurHash3.stringHash(arg, 1883427247) & 0x7FFFFFFF
    val hash_udf = udf[Int, String](hash_fun)


    def geo_fun(arg: String, i: Integer): String = { val a = arg.split("[>]", -1) ++ Seq("", "", ""); a(i) }
    
    def geo_country(arg: String): String = geo_fun(arg, 0)
    def geo_state(arg: String): String = geo_fun(arg, 1)
    def geo_dma(arg: String): String = geo_fun(arg, 2)

    val geo_country_udf = udf[String, String](geo_country)
    val geo_state_udf = udf[String, String](geo_state)
    val geo_dma_udf = udf[String, String](geo_dma)

    def dow_fun(arg: Int): Int = ((arg / 1000) / 3600 / 24) % 7
    val dow_udf = udf[Int, Int](dow_fun)

    def dot_fun(arg1: Seq[Double], arg2: Seq[Double]): Double = arg1.zip(arg2).map(x => x._1 * x._2).foldLeft(0d)(_+_)
    val dot_udf = udf[Double, Seq[Double], Seq[Double]](dot_fun)

    val conf = new SparkConf().setAppName("Train")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    val continents = sqlContext.read.parquet(path_continent_parquet).cache

    val events = sqlContext.read.parquet(path_events_traffic_parquet)
      .withColumn("platform", when(expr("platform IN (1,2,3)"), col("platform")).otherwise(2).as("platform"))
      .withColumn("traffic_source", when(expr("traffic_source IN (1,2,3)"), col("traffic_source")).otherwise(1).as("traffic_source"))
      .withColumn("geo_country", geo_country_udf(col("geo_location")).as("geo_country"))
      .withColumn("geo_country", when(col("geo_country") === "\\N" || col("geo_country") === "--", "US").otherwise(col("geo_country")))
      .withColumn("geo_state", geo_state_udf(col("geo_location")).as("geo_state"))
      .withColumn("geo_dma", geo_dma_udf(col("geo_location")).as("geo_dma"))
      .withColumn("hash", hash_udf(col("uuid")).as("hash"))
      .withColumn("dow", dow_udf(col("timestamp")).as("dow"))
      .join(continents, Seq("geo_country"), "left_outer")
      // .filter(expr("(hash % 11 IN (4, 3, 9))"))

    val clicks = sqlContext.read.parquet(path_clicks_parquet)

    val click_events = clicks.join(events, Seq("display_id"))

    val doc_meta = sqlContext.read.parquet(path_doc_meta_parquet)
    val doc_categories = sqlContext.read.parquet(path_doc_categories_parquet)

    val promoted_content = sqlContext.read.parquet(path_promoted_content_parquet)
      .drop("document_id")
      .distinct
      .cache

    val ad_ctr = sqlContext.read.parquet(path_ad_ctr_parquet)
    val doc_ctr = sqlContext.read.parquet(path_doc_ctr_parquet)
    val platform_ctr = sqlContext.read.parquet(path_platform_ctr_parquet)
    val traffic_source_ctr = sqlContext.read.parquet(path_traffic_source_ctr_parquet) 
    val dow_ctr = sqlContext.read.parquet(path_dow_ctr_parquet)
    val category_ctr = sqlContext.read.parquet(path_category_ctr_parquet)
    val continent_ctr = sqlContext.read.parquet(path_continent_ctr_parquet) 
    val country_ctr = sqlContext.read.parquet(path_country_ctr_parquet) 
    val state_ctr = sqlContext.read.parquet(path_state_ctr_parquet) 
    val dma_ctr = sqlContext.read.parquet(path_dma_ctr_parquet) 
    val publisher_ctr = sqlContext.read.parquet(path_publisher_ctr_parquet) 
    val source_ctr = sqlContext.read.parquet(path_source_ctr_parquet)
    val campaign_ctr = sqlContext.read.parquet(path_campaign_ctr_parquet)
    val advertiser_ctr = sqlContext.read.parquet(path_advertiser_ctr_parquet) 

    val user_seen_ad_ts = sqlContext.read.parquet(path_user_seen_ad_ts)
    var user_seen_campaign_ts = sqlContext.read.parquet(path_user_seen_campaign_ts)
    var user_click_ad_ts = sqlContext.read.parquet(path_user_click_ad_ts)
    var user_click_campaign_ts = sqlContext.read.parquet(path_user_click_campaign_ts)

    var user_profile = sqlContext.read.parquet(path_user_profile)

    var ad_center = sqlContext.read.parquet(path_ad_center)
    var campaign_center = sqlContext.read.parquet(path_campaign_center)


    val click_camapign_ind = click_events
      .select("display_id", "ad_id")
      .join(promoted_content, Seq("ad_id"), "left_outer")
      .select("display_id", "ad_id", "campaign_id")
      .distinct
      .withColumn("camp_17295", when(expr("campaign_id = '17295'"), 1).otherwise(0))
      .withColumn("camp_674", when(expr("campaign_id = '674'"), 1).otherwise(0))      
      .withColumn("camp_104", when(expr("campaign_id = '104'"), 1).otherwise(0))
      .withColumn("camp_17402", when(expr("campaign_id = '17402'"), 1).otherwise(0))
      .withColumn("camp_7283", when(expr("campaign_id = '7283'"), 1).otherwise(0))
      .withColumn("camp_21143", when(expr("campaign_id = '21143'"), 1).otherwise(0))
      .withColumn("camp_22477", when(expr("campaign_id = '22477'"), 1).otherwise(0))
      .withColumn("camp_21149", when(expr("campaign_id = '21149'"), 1).otherwise(0))
      .withColumn("camp_15885", when(expr("campaign_id = '15885'"), 1).otherwise(0))
      .withColumn("camp_15889", when(expr("campaign_id = '15889'"), 1).otherwise(0))
      .withColumn("camp_25012", when(expr("campaign_id = '25012'"), 1).otherwise(0))
      .withColumn("camp_22843", when(expr("campaign_id = '22843'"), 1).otherwise(0))
      .withColumn("camp_15430", when(expr("campaign_id = '15430'"), 1).otherwise(0))
      .withColumn("camp_24152", when(expr("campaign_id = '24152'"), 1).otherwise(0))
      .withColumn("camp_17873", when(expr("campaign_id = '17873'"), 1).otherwise(0))
      .withColumn("camp_26260", when(expr("campaign_id = '26260'"), 1).otherwise(0))
      .withColumn("camp_27878", when(expr("campaign_id = '27878'"), 1).otherwise(0))
      .withColumn("camp_2359", when(expr("campaign_id = '12359'"), 1).otherwise(0))
      .withColumn("camp_18075", when(expr("campaign_id = '18075'"), 1).otherwise(0))
      .withColumn("camp_30017", when(expr("campaign_id = '30017'"), 1).otherwise(0))
      .withColumn("camp_16563", when(expr("campaign_id = '16563'"), 1).otherwise(0))
      .withColumn("camp_443", when(expr("campaign_id = '443'"), 1).otherwise(0))
      .withColumn("camp_16636", when(expr("campaign_id = '16636'"), 1).otherwise(0))
      .withColumn("camp_31931", when(expr("campaign_id = '31931'"), 1).otherwise(0))
      .withColumn("camp_15431", when(expr("campaign_id = '15431'"), 1).otherwise(0))
      .withColumn("camp_10945", when(expr("campaign_id = '10945'"), 1).otherwise(0))
      .withColumn("camp_5043", when(expr("campaign_id = '5043'"), 1).otherwise(0))
      .withColumn("camp_6235", when(expr("campaign_id = '6235'"), 1).otherwise(0))
      .withColumn("camp_7615", when(expr("campaign_id = '7615'"), 1).otherwise(0))
      .withColumn("camp_15214", when(expr("campaign_id = '15214'"), 1).otherwise(0))
      .withColumn("camp_7978", when(expr("campaign_id = '7978'"), 1).otherwise(0))
      .drop("campaign_id")

    val click_publisher_ind = click_events
      .select("display_id", "document_id")
      .join(doc_meta, Seq("document_id"), "left_outer")
      .select("display_id", "publisher_id")
      .distinct
      .withColumn("pub_450", when(expr("publisher_id = '450'"), 1).otherwise(0))
      .withColumn("pub_1089",  when(expr("publisher_id = '1089'"), 1).otherwise(0))
      .withColumn("pub_874",  when(expr("publisher_id = '874'"), 1).otherwise(0))
      .withColumn("pub_433",  when(expr("publisher_id = '433'"), 1).otherwise(0))
      .withColumn("pub_1042",  when(expr("publisher_id = '1042'"), 1).otherwise(0))
      .withColumn("pub_240",  when(expr("publisher_id = '240'"), 1).otherwise(0))
      .withColumn("pub_180",  when(expr("publisher_id = '180'"), 1).otherwise(0))
      .withColumn("pub_41", when(expr("publisher_id = '41'"), 1).otherwise(0))
      .withColumn("pub_118",  when(expr("publisher_id = '118'"), 1).otherwise(0))
      .withColumn("pub_236",  when(expr("publisher_id = '236'"), 1).otherwise(0))
      .withColumn("pub_78", when(expr("publisher_id = '78'"), 1).otherwise(0))
      .withColumn("pub_407",  when(expr("publisher_id = '407'"), 1).otherwise(0))
      .withColumn("pub_206",  when(expr("publisher_id = '206'"), 1).otherwise(0))
      .withColumn("pub_714",  when(expr("publisher_id = '714'"), 1).otherwise(0))
      .withColumn("pub_468",  when(expr("publisher_id = '478'"), 1).otherwise(0))
      .withColumn("pub_740",  when(expr("publisher_id = '740'"), 1).otherwise(0))
      .withColumn("pub_867",  when(expr("publisher_id = '867'"), 1).otherwise(0))
      .withColumn("pub_1228",  when(expr("publisher_id = '1228'"), 1).otherwise(0))
      .withColumn("pub_875",  when(expr("publisher_id = '875'"), 1).otherwise(0))
      .withColumn("pub_184",  when(expr("publisher_id = '184'"), 1).otherwise(0))
      .withColumn("pub_1066",  when(expr("publisher_id = '1066'"), 1).otherwise(0))
      .withColumn("pub_58", when(expr("publisher_id = '58'"), 1).otherwise(0))
      .withColumn("pub_440",  when(expr("publisher_id = '440'"), 1).otherwise(0))
      .withColumn("pub_320",  when(expr("publisher_id = '320'"), 1).otherwise(0))
      .withColumn("pub_435",  when(expr("publisher_id = '435'"), 1).otherwise(0))
      .withColumn("pub_26", when(expr("publisher_id = '26'"), 1).otherwise(0))
      .withColumn("pub_151",  when(expr("publisher_id = '151'"), 1).otherwise(0))
      .drop("publisher_id")


    val click_geo_ind = click_events
      .select("display_id", "geo_country", "geo_state")
      .distinct
      .withColumn("geo_US_CA", when(expr("geo_country = 'US' AND geo_state = 'CA'"), 1).otherwise(0))
      .withColumn("geo_US_TX", when(expr("geo_country = 'US' AND geo_state = 'TX'"), 1).otherwise(0))
      .withColumn("geo_US_NY", when(expr("geo_country = 'US' AND geo_state = 'NY'"), 1).otherwise(0))
      .withColumn("geo_US_FL", when(expr("geo_country = 'US' AND geo_state = 'FL'"), 1).otherwise(0))
      .withColumn("geo_US_PA", when(expr("geo_country = 'US' AND geo_state = 'PA'"), 1).otherwise(0))
      .withColumn("geo_US_IL", when(expr("geo_country = 'US' AND geo_state = 'IL'"), 1).otherwise(0))
      .withColumn("geo_US_OH", when(expr("geo_country = 'US' AND geo_state = 'OH'"), 1).otherwise(0))
      .withColumn("geo_US_NC", when(expr("geo_country = 'US' AND geo_state = 'NC'"), 1).otherwise(0))
      .withColumn("geo_US_MI", when(expr("geo_country = 'US' AND geo_state = 'MI'"), 1).otherwise(0))
      .withColumn("geo_CA_ON", when(expr("geo_country = 'CA' AND geo_state = 'ON'"), 1).otherwise(0))
      .withColumn("geo_US_NJ", when(expr("geo_country = 'US' AND geo_state = 'NJ'"), 1).otherwise(0))
      .drop("geo_country")
      .drop("geo_state")


    val click_user_profile = click_events
      .select("display_id", "ad_id", "uuid")
      .join(user_profile, Seq("uuid"))

    val click_ad_center = click_user_profile
      .join(ad_center, Seq("ad_id"))
      .withColumn("user_ad_category_clicked", dot_udf(col("category_vector"), col("ad_category_center_clicked")))
      .withColumn("user_ad_category_nonclicked", dot_udf(col("category_vector"), col("ad_category_center_nonclicked")))
      .withColumn("user_ad_topic_clicked", dot_udf(col("topic_vector"), col("ad_topic_center_clicked")))
      .withColumn("user_ad_topic_nonclicked", dot_udf(col("topic_vector"), col("ad_topic_center_nonclicked")))
      .withColumn("user_ad_entity_clicked", dot_udf(col("entity_vector"), col("ad_entity_center_clicked")))
      .withColumn("user_ad_entity_nonclicked", dot_udf(col("entity_vector"), col("ad_entity_center_nonclicked")))
      .select("display_id", "ad_id",
        "user_ad_category_clicked",
        "user_ad_category_nonclicked",
        "user_ad_topic_clicked",
        "user_ad_topic_nonclicked",
        "user_ad_entity_clicked",
        "user_ad_entity_nonclicked")

    val click_campaign_center = click_user_profile
      .join(promoted_content, Seq("ad_id"))
      .filter("campaign_id != ''")
      .join(campaign_center, Seq("campaign_id"))
      .withColumn("user_campaign_category_clicked", dot_udf(col("category_vector"), col("campaign_category_center_clicked")))
      .withColumn("user_campaign_category_nonclicked", dot_udf(col("category_vector"), col("campaign_category_center_nonclicked")))
      .withColumn("user_campaign_topic_clicked", dot_udf(col("topic_vector"), col("campaign_topic_center_clicked")))
      .withColumn("user_campaign_topic_nonclicked", dot_udf(col("topic_vector"), col("campaign_topic_center_nonclicked")))
      .withColumn("user_campaign_entity_clicked", dot_udf(col("entity_vector"), col("campaign_entity_center_clicked")))
      .withColumn("user_campaign_entity_nonclicked", dot_udf(col("entity_vector"), col("campaign_entity_center_nonclicked")))
      .select("display_id", "ad_id",
        "user_campaign_category_clicked",
        "user_campaign_category_nonclicked",
        "user_campaign_topic_clicked",
        "user_campaign_topic_nonclicked",
        "user_campaign_entity_clicked",
        "user_campaign_entity_nonclicked")

    val click_center = click_ad_center
      .join(click_campaign_center, Seq("display_id", "ad_id"), "left_outer")
      .select(col("display_id"), col("ad_id"),
        col("user_ad_category_clicked"),
        col("user_ad_category_nonclicked"),
        col("user_ad_topic_clicked"),
        col("user_ad_topic_nonclicked"),
        col("user_ad_entity_clicked"),
        col("user_ad_entity_nonclicked"),
        expr("COALESCE(user_campaign_category_clicked, user_ad_category_clicked)").as("user_campaign_category_clicked"),
        expr("COALESCE(user_campaign_category_nonclicked, user_ad_category_nonclicked)").as("user_campaign_category_nonclicked"),
        expr("COALESCE(user_campaign_topic_clicked, user_ad_topic_clicked)").as("user_campaign_topic_clicked"),
        expr("COALESCE(user_campaign_topic_nonclicked, user_ad_topic_nonclicked)").as("user_campaign_topic_nonclicked"),
        expr("COALESCE(user_campaign_entity_clicked, user_ad_entity_clicked)").as("user_campaign_entity_clicked"),
        expr("COALESCE(user_campaign_entity_nonclicked, user_ad_entity_nonclicked)").as("user_campaign_entity_nonclicked"))

    val click_user_seen_ind = click_events
      .join(promoted_content, Seq("ad_id"), "left_outer")
      .join(user_seen_ad_ts, Seq("ad_id", "uuid"), "left_outer")
      .join(user_seen_campaign_ts, Seq("campaign_id", "uuid"), "left_outer")
      .withColumn("ad_seen_before", expr("(COALESCE(min_ad_timestamp, timestamp) < timestamp)").cast(IntegerType))
      .withColumn("ad_seen_after", expr("(COALESCE(max_ad_timestamp, timestamp) > timestamp)").cast(IntegerType))
      .withColumn("campaign_seen_before", expr("(COALESCE(min_campaign_timestamp, timestamp) < timestamp)").cast(IntegerType))
      .withColumn("campaign_seen_after", expr("(COALESCE(max_campaign_timestamp, timestamp) > timestamp)").cast(IntegerType))
      .select("display_id",
        "ad_id",
        "ad_seen_before",
        "ad_seen_after",
        "campaign_seen_before",
        "campaign_seen_after")

    val click_user_click_ind = click_events
      .join(promoted_content, Seq("ad_id"), "left_outer")
      .join(user_click_ad_ts, Seq("ad_id", "uuid"), "left_outer")
      .join(user_click_campaign_ts, Seq("campaign_id", "uuid"), "left_outer")
      .withColumn("ad_click_before", expr("(COALESCE(min_ad_timestamp, timestamp) < timestamp)").cast(IntegerType))
      .withColumn("ad_click_after", expr("(COALESCE(max_ad_timestamp, timestamp) > timestamp)").cast(IntegerType))
      .withColumn("campaign_click_before", expr("(COALESCE(min_campaign_timestamp, timestamp) < timestamp)").cast(IntegerType))
      .withColumn("campaign_click_after", expr("(COALESCE(max_campaign_timestamp, timestamp) > timestamp)").cast(IntegerType))
      .select("display_id",
        "ad_id",
        "ad_click_before",
        "ad_click_after",
        "campaign_click_before",
        "campaign_click_after")

    val click_category_ctr = click_events
      .join(doc_categories, Seq("document_id"))
      .join(category_ctr, Seq("ad_id", "category_id"))
      .groupBy("display_id", "ad_id")
      .agg(sum(expr("confidence_level * category_clicks / category_impressions")).as("sum_category_ctr"),
        sum("confidence_level").as("sum_confidence_level"))
      .select(col("display_id"),
        col("ad_id"),
        expr("sum_category_ctr / sum_confidence_level").as("category_ctr"))

   val click_category_avg_ctr = click_category_ctr
     .groupBy("ad_id")
     .agg(avg("category_ctr").as("category_avg_ctr"))

    val click_geo_ctr = click_events
      .join(continent_ctr, Seq("geo_continent", "ad_id"), "left_outer")
      .join(country_ctr, Seq("geo_continent", "geo_country", "ad_id"), "left_outer")
      .join(state_ctr, Seq("geo_continent", "geo_country", "geo_state", "ad_id"), "left_outer")
      .join(dma_ctr, Seq("geo_continent", "geo_country", "geo_state", "geo_dma", "ad_id"), "left_outer")
      .withColumn("continent_sign", expr("COALESCE(continent_impressions, -1) / COALESCE(continent_impressions, 1)"))
      .withColumn("country_sign", expr("COALESCE(country_impressions, -1) / COALESCE(country_impressions, 1)"))
      .withColumn("state_sign", expr("COALESCE(state_impressions, -1) / COALESCE(state_impressions, 1)"))
      .withColumn("dma_sign", expr("COALESCE(dma_impressions, -1) / COALESCE(dma_impressions, 1)"))
      .withColumn("continent_clicks", expr("COALESCE(continent_clicks, 0)"))
      .withColumn("continent_impressions", expr("COALESCE(continent_impressions, 0)"))
      .withColumn("country_clicks", expr("COALESCE(country_clicks, continent_clicks)"))
      .withColumn("country_impressions", expr("COALESCE(country_impressions, continent_impressions)"))
      .withColumn("state_clicks", expr("COALESCE(state_clicks, country_clicks)"))
      .withColumn("state_impressions", expr("COALESCE(state_impressions, country_impressions)"))
      .withColumn("dma_clicks", expr("COALESCE(dma_clicks, state_clicks)"))
      .withColumn("dma_impressions", expr("COALESCE(dma_impressions, state_impressions)"))
      .withColumn("continent_ctr", expr("continent_sign * (continent_clicks + 10) / (continent_impressions + 100)"))
      .withColumn("country_ctr", expr("country_sign * (country_clicks + 10) / (country_impressions + 100)"))
      .withColumn("state_ctr", expr("state_sign * (state_clicks + 10) / (state_impressions + 100)"))
      .withColumn("dma_ctr", expr("dma_sign * (dma_clicks + 10) / (dma_impressions + 100)"))
      .select(col("display_id"),
        col("ad_id"),
        col("continent_ctr"),
        col("country_ctr"),
        col("state_ctr"),
        col("dma_ctr"))

    val click_traffic_source_ctr = click_events
      .join(traffic_source_ctr, Seq("ad_id", "traffic_source"), "left_outer")
      .withColumn("traffic_source_sign", expr("COALESCE(traffic_source_impressions, -1) / COALESCE(traffic_source_impressions, 1)"))
      .withColumn("traffic_source_ctr", expr("traffic_source_sign * (COALESCE(traffic_source_clicks, 0) + 10) / (COALESCE(traffic_source_impressions, 0) + 100)"))
      .select(col("display_id"),
        col("ad_id"),
        col("traffic_source_ctr"))

    val click_platform_ctr = click_events
      .join(platform_ctr, Seq("ad_id", "platform"), "left_outer")
      .withColumn("platform_sign", expr("COALESCE(platform_impressions, -1) / COALESCE(platform_impressions, 1)"))
      .withColumn("platform_ctr", expr("platform_sign * (COALESCE(platform_clicks, 0) + 10) / (COALESCE(platform_impressions, 0) + 100)"))
      .select(col("display_id"),
        col("ad_id"),
        col("platform_ctr"))

    val click_dow_ctr = click_events
      .join(dow_ctr, Seq("ad_id", "dow"), "left_outer")
      .withColumn("dow_sign", expr("COALESCE(dow_impressions, -1) / COALESCE(dow_impressions, 1)"))
      .withColumn("dow_ctr", expr("dow_sign * (COALESCE(dow_clicks, 0) + 10) / (COALESCE(dow_impressions, 0) + 100)"))
      .select(col("display_id"),
        col("ad_id"),
        col("dow_ctr"))

    val click_doc_ctr = click_events // DONE: check if publisher provided
      .join(doc_meta, Seq("document_id"), "left_outer")
      .join(publisher_ctr, Seq("ad_id", "publisher_id"), "left_outer")
      .join(source_ctr, Seq("ad_id", "publisher_id", "source_id"), "left_outer")
      .join(doc_ctr, Seq("ad_id", "document_id"), "left_outer")
      .withColumn("publisher_sign", expr("COALESCE(publisher_impressions, -1) / COALESCE(publisher_impressions, 1)"))
      .withColumn("source_sign", expr("COALESCE(source_impressions, -1) / COALESCE(source_impressions, 1)"))
      .withColumn("doc_sign", expr("COALESCE(doc_impressions, -1) / COALESCE(doc_impressions, 1)"))
      .withColumn("doc_clicks", when(col("publisher_clicks").isNull, expr("COALESCE(doc_clicks, 0)")).otherwise(expr("COALESCE(doc_clicks, publisher_clicks)")))
      .withColumn("doc_impressions", when(col("publisher_impressions").isNull, expr("COALESCE(doc_impressions, 0)")).otherwise(expr("COALESCE(doc_impressions, publisher_impressions)")))
      .withColumn("publisher_clicks", expr("COALESCE(publisher_clicks, doc_clicks)"))
      .withColumn("publisher_impressions", expr("COALESCE(publisher_impressions, doc_impressions)"))
      .withColumn("source_clicks", expr("COALESCE(source_clicks, publisher_clicks)"))
      .withColumn("source_impressions", expr("COALESCE(source_impressions, publisher_impressions)"))
      .withColumn("doc_ctr", expr("doc_sign * (doc_clicks + 10) / (doc_impressions + 100)"))
      .withColumn("publisher_ctr", expr("publisher_sign * (publisher_clicks + 10) / (publisher_impressions + 100)"))
      .withColumn("source_ctr", expr("source_sign * (source_clicks + 10) / (source_impressions + 100)"))
      .select(col("display_id"),
        col("ad_id"),
        col("doc_ctr"),
        col("publisher_ctr"),
        col("source_ctr"))

    val click_ad_ctr = click_events // DONE: check if advertiser provided
      .join(promoted_content, Seq("ad_id"), "left_outer")
      .join(advertiser_ctr, Seq("advertiser_id"), "left_outer")
      .join(campaign_ctr, Seq("advertiser_id", "campaign_id"), "left_outer")
      .join(ad_ctr, Seq("advertiser_id", "campaign_id", "ad_id"), "left_outer")
      .withColumn("advertiser_sign", expr("COALESCE(advertiser_impressions, -1) / COALESCE(advertiser_impressions, 1)"))
      .withColumn("campaign_sign", expr("COALESCE(campaign_impressions, -1) / COALESCE(campaign_impressions, 1)"))
      .withColumn("ad_sign", expr("COALESCE(ad_impressions, -1) / COALESCE(ad_impressions, 1)"))
      .withColumn("ad_clicks", when(col("campaign_clicks").isNull, expr("COALESCE(ad_clicks, 0)")).otherwise(expr("COALESCE(ad_clicks, campaign_clicks)")))
      .withColumn("ad_impressions", when(col("campaign_impressions").isNull, expr("COALESCE(ad_impressions, 0)")).otherwise(expr("COALESCE(ad_impressions, campaign_impressions)")))
      .withColumn("campaign_clicks", expr("COALESCE(campaign_clicks, ad_clicks)"))
      .withColumn("campaign_impressions", expr("COALESCE(campaign_impressions, ad_impressions)"))
      .withColumn("advertiser_clicks", expr("COALESCE(advertiser_clicks, campaign_clicks)"))
      .withColumn("advertiser_impressions", expr("COALESCE(advertiser_impressions, campaign_impressions)"))
      .withColumn("ad_ctr", expr("ad_sign * (ad_clicks + 10) / (ad_impressions + 100)"))
      .withColumn("campaign_ctr", expr("campaign_sign * (campaign_clicks + 10) / (campaign_impressions + 100)"))
      .withColumn("advertiser_ctr", expr("advertiser_sign * (advertiser_clicks + 10) / (advertiser_impressions + 100)"))
      .select(col("display_id"),
        col("ad_id"),
        col("ad_ctr"),
        col("campaign_ctr"),
        col("advertiser_ctr"))

    click_events
      .withColumn("platform_ind_1", when(expr("platform = 1"), 1).otherwise(0))
      .withColumn("platform_ind_2", when(expr("platform = 2"), 1).otherwise(0))
      .withColumn("platform_ind_3", when(expr("platform = 3"), 1).otherwise(0))
      .withColumn("traffic_source_ind_1", when(expr("traffic_source = 1"), 1).otherwise(0))
      .withColumn("traffic_source_ind_2", when(expr("traffic_source = 2"), 1).otherwise(0))
      .withColumn("traffic_source_ind_3", when(expr("traffic_source = 3"), 1).otherwise(0))
      .select("display_id", "ad_id", "hash", "clicked",
        "platform_ind_1", "platform_ind_2", "platform_ind_3", 
        "traffic_source_ind_1", "traffic_source_ind_2", "traffic_source_ind_3")
      .join(click_camapign_ind, Seq("display_id", "ad_id"), "left_outer")
      .join(click_publisher_ind, Seq("display_id"), "left_outer")
      .join(click_geo_ind, Seq("display_id"), "left_outer")
      .join(click_user_seen_ind, Seq("display_id", "ad_id"), "left_outer")
      .join(click_user_click_ind, Seq("display_id", "ad_id"), "left_outer")
      .join(click_category_ctr, Seq("display_id", "ad_id"), "left_outer")
      .join(click_category_avg_ctr, Seq("ad_id"), "left_outer")
      .join(click_geo_ctr, Seq("display_id", "ad_id"), "left_outer")
      .join(click_traffic_source_ctr, Seq("display_id", "ad_id"), "left_outer")
      .join(click_platform_ctr, Seq("display_id", "ad_id"), "left_outer")
      .join(click_dow_ctr, Seq("display_id", "ad_id"), "left_outer")
      .join(click_doc_ctr, Seq("display_id", "ad_id"), "left_outer")
      .join(click_ad_ctr, Seq("display_id", "ad_id"), "left_outer")
      .join(click_center, Seq("display_id", "ad_id"), "left_outer")
      .select(col("display_id"),
        col("ad_id"),
        col("hash"),
        col("clicked"),
        expr("COALESCE(category_ctr, COALESCE(category_avg_ctr, -0.1))").as("category_ctr"),
        col("continent_ctr"),
        col("country_ctr"),
        col("state_ctr"),
        col("dma_ctr"),
        col("traffic_source_ctr"),
        col("platform_ctr"),
        col("dow_ctr"),
        col("doc_ctr"),
        col("source_ctr"),
        col("publisher_ctr"),
        col("ad_ctr"),
        col("campaign_ctr"),
        col("advertiser_ctr"),
        col("ad_seen_before"),
        col("ad_seen_after"),
        col("campaign_seen_before"),
        col("campaign_seen_after"),
        col("ad_click_before"),
        col("ad_click_after"),
        col("campaign_click_before"),
        col("campaign_click_after"),
        expr("COALESCE(user_campaign_category_clicked, 0)").as("user_campaign_category_clicked"),
        expr("COALESCE(user_campaign_category_nonclicked, 0)").as("user_campaign_category_nonclicked"),
        expr("COALESCE(user_campaign_topic_clicked, 0)").as("user_campaign_topic_clicked"),
        expr("COALESCE(user_campaign_topic_nonclicked, 0)").as("user_campaign_topic_nonclicked"),
        expr("COALESCE(user_campaign_entity_clicked, 0)").as("user_campaign_entity_clicked"),
        expr("COALESCE(user_campaign_entity_nonclicked, 0)").as("user_campaign_entity_nonclicked"),
        expr("COALESCE(user_ad_category_clicked, 0)").as("user_ad_category_clicked"),
        expr("COALESCE(user_ad_category_nonclicked, 0)").as("user_ad_category_nonclicked"),
        expr("COALESCE(user_ad_topic_clicked, 0)").as("user_ad_topic_clicked"),
        expr("COALESCE(user_ad_topic_nonclicked, 0)").as("user_ad_topic_nonclicked"),
        expr("COALESCE(user_ad_entity_clicked, 0)").as("user_ad_entity_clicked"),
        expr("COALESCE(user_ad_entity_nonclicked, 0)").as("user_ad_entity_nonclicked"),
        col("camp_17295"),
        col("camp_674"),
        col("camp_104"),
        col("camp_17402"),
        col("camp_7283"),
        col("camp_21143"),
        col("camp_22477"),
        col("camp_21149"),
        col("camp_15885"),
        col("camp_15889"),
        col("camp_25012"),
        col("camp_22843"),
        col("camp_15430"),
        col("camp_24152"),
        col("camp_17873"),
        col("camp_26260"),
        col("camp_27878"),
        col("camp_2359"),
        col("camp_18075"),
        col("camp_30017"),
        col("camp_16563"),
        col("camp_443"),
        col("camp_16636"),
        col("camp_31931"),
        col("camp_15431"),
        col("camp_10945"),
        col("camp_5043"),
        col("camp_6235"),
        col("camp_7615"),
        col("camp_15214"),
        col("camp_7978"),
        col("pub_450"),
        col("pub_1089"),
        col("pub_874"),
        col("pub_433"),
        col("pub_1042"),
        col("pub_240"),
        col("pub_180"),
        col("pub_41"),
        col("pub_118"),
        col("pub_236"),
        col("pub_78"),
        col("pub_407"),
        col("pub_206"),
        col("pub_714"),
        col("pub_468"),
        col("pub_740"),
        col("pub_867"),
        col("pub_1228"),
        col("pub_875"),
        col("pub_184"),
        col("pub_1066"),
        col("pub_58"),
        col("pub_440"),
        col("pub_320"),
        col("pub_435"),
        col("pub_26"),
        col("pub_151"),
        col("geo_US_CA"),
        col("geo_US_TX"),
        col("geo_US_NY"),
        col("geo_US_FL"),
        col("geo_US_PA"),
        col("geo_US_IL"),
        col("geo_US_OH"),
        col("geo_US_NC"),
        col("geo_US_MI"),
        col("geo_CA_ON"),
        col("geo_US_NJ"),
        col("platform_ind_1"),
        col("platform_ind_2"),
        col("platform_ind_3"),
        col("traffic_source_ind_1"),
        col("traffic_source_ind_2"),
        col("traffic_source_ind_3"))
      .orderBy(col("display_id"))
      .repartition(1)
      .write.parquet(path_result_parquet)

    val result = sqlContext.read.parquet(path_result_parquet)

    println(result.count)
    result.take(1000).foreach{e:Row => println(e.mkString("\t"))}

    sc.stop()
  }
}
