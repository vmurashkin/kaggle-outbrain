import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import scala.util.hashing.MurmurHash3
import scala.collection.mutable.WrappedArray
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import org.apache.spark.rdd.RDD


object UserProfile {

  case class Click(display_id: String, ad_id: String, clicked: Integer)
  case class PageView(uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String, traffic_source: Integer)
  case class Event(display_id: String, uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String)
  case class DocCategory(document_id: String, category_id: String, confidence_level: Double)
  case class DocEntity(document_id: String, entity_id: String, confidence_level: Double)
  case class DocMeta(document_id: String, source_id: String, publisher_id: String, publish_time: String)
  case class DocTopic(document_id: String, topic_id: String, confidence_level: Double)
  case class PromotedContent(ad_id: String, document_id: String, campaign_id: String, advertiser_id: String)

  object mkVectorUDF extends UserDefinedAggregateFunction {
      // Schema you get as an input
      def inputSchema = new StructType()
        .add("key", StringType)
        .add("value", DoubleType)

      // Schema of the row which is used for aggregation
      def bufferSchema = new StructType()
        .add("vector", ArrayType(DoubleType))

      // Returned type
      def dataType = ArrayType(DoubleType)
      // Self-explaining
      def deterministic = true
      // zero value
      def initialize(buffer: MutableAggregationBuffer) = {
        buffer.update(0, List.fill(17)(0d).toArray)
      }
      // Similar to seqOp in aggregate
      def update(buffer: MutableAggregationBuffer, input: Row) = {
          if (!input.isNullAt(0) && !input.isNullAt(1)) {
            val vector = buffer.getAs[WrappedArray[Double]](0)
            val hash = MurmurHash3.stringHash(input.getString(0), 1883427247) & 0x7FFFFFFF
            vector(hash % 17) += input.getDouble(1)
            buffer.update(0, vector)
          }
      }
      // Similar to combOp in aggregate
      def merge(buffer1: MutableAggregationBuffer, buffer2: Row) = {
        buffer1.update(0, buffer1.getAs[Seq[Double]](0).zip(buffer2.getAs[Seq[Double]](0)).map(x => x._1 + x._2).toArray) 
      }

      // Called on exit to get return value
      def evaluate(buffer: Row) = {
        val vector = buffer.getAs[Seq[Double]](0)
        val norm = math.pow(vector.map(math.pow(_, 2d)).foldLeft(0d)(_+_), 0.5d)
        vector.map(_ / norm).toArray
      }
  }


  def main(args: Array[String]) {

    var path_continent_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/continent_parquet"
    var path_clicks_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train_parquet"
    var path_clicks_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test_parquet"
    var path_events_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_parquet"
    var path_page_views_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views_parquet"
    var path_doc_categories_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories_parquet"
    var path_doc_entities_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities_parquet"
    var path_doc_meta_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta_parquet"
    var path_doc_topics_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics_parquet"
    var path_promoted_content_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content_parquet"

    var path_events_traffic_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_traffic_parquet"

    var path_user_profile_category_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_category_parquet"
    var path_user_profile_topic_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_topic_parquet"
    var path_user_profile_topic_parquet_0 = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_topic_parquet_0"
    var path_user_profile_topic_parquet_1 = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_topic_parquet_1"
    var path_user_profile_entity_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_entity_parquet"
    var path_user_profile_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_parquet"
    var path_user_profile_parquet_0 = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_parquet_0"
    var path_user_profile_parquet_1 = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_parquet_1"

    val path_doc_topics_hash_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/67/doc_topics_hash_parquet"

    def hash_fun(arg: String): Int = MurmurHash3.stringHash(arg, 1883427247) & 0x7FFFFFFF
    val hash_udf = udf[Int, String](hash_fun)


    val conf = new SparkConf().setAppName("UserProfile")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    val page_views = sqlContext.read.parquet(path_page_views_parquet)

    val doc_categories = sqlContext.read.parquet(path_doc_categories_parquet)
    val doc_entities = sqlContext.read.parquet(path_doc_entities_parquet)
    val doc_topics = sqlContext.read.parquet(path_doc_topics_parquet)

    // page_views
    //   .select("uuid", "document_id")
    //   .join(doc_categories, Seq("document_id"))
    //   .groupBy("uuid")
    //   .agg(mkVectorUDF(col("category_id"), col("confidence_level")).as("vector"))
    //   .repartition(16, col("uuid"))
    //   .write.parquet(path_user_profile_category_parquet)

    // page_views
    //   .select("uuid", "document_id")
    //   .join(doc_entities, Seq("document_id"))
    //   .groupBy("uuid")
    //   .agg(mkVectorUDF(col("entity_id"), col("confidence_level")).as("vector"))
    //   .repartition(16, col("uuid"))
    //   .write.parquet(path_user_profile_entity_parquet)

    // sqlContext.read.parquet(path_user_profile_entity_parquet).take(100).foreach{e:Row => println(e.mkString(" "))}

    // page_views
    //   .withColumn("hash", hash_udf(col("uuid")))
    //   .filter("(hash % 2) = 0")
    //   .select("uuid", "document_id")
    //   .join(doc_topics, Seq("document_id"))
    //   .groupBy("uuid")
    //   .agg(mkVectorUDF(col("topic_id"), col("confidence_level")).as("vector"))
    //   .repartition(16, col("uuid"))
    //   .write.parquet(path_user_profile_topic_parquet_0)

    // sqlContext.read.parquet(path_user_profile_topic_parquet_0).take(100).foreach{e:Row => println(e.mkString(" "))}

    // page_views
    //   .withColumn("hash", hash_udf(col("uuid")))
    //   .filter("(hash % 2) = 1")
    //   .select("uuid", "document_id")
    //   .join(doc_topics, Seq("document_id"))
    //   .groupBy("uuid")
    //   .agg(mkVectorUDF(col("topic_id"), col("confidence_level")).as("vector"))
    //   .repartition(16, col("uuid"))
    //   .write.parquet(path_user_profile_topic_parquet_1)

    // sqlContext.read.parquet(path_user_profile_topic_parquet_1).take(100).foreach{e:Row => println(e.mkString(" "))}


    // val user_profile_category = sqlContext.read.parquet(path_user_profile_category_parquet)
    //   .withColumn("hash", hash_udf(col("uuid")))
    //   .filter("(hash % 2) = 0")
    //   .drop("hash")
    //   .withColumnRenamed("vector", "category_vector")

    // val user_profile_entity = sqlContext.read.parquet(path_user_profile_entity_parquet)
    //   .withColumn("hash", hash_udf(col("uuid")))
    //   .filter("(hash % 2) = 0")
    //   .drop("hash")
    //   .withColumnRenamed("vector", "entity_vector")

    // val user_profile_topic = sqlContext.read.parquet(path_user_profile_topic_parquet_0)
    //   .withColumnRenamed("vector", "topic_vector")

    // user_profile_category
    //   .join(user_profile_topic, Seq("uuid"))
    //   .join(user_profile_entity, Seq("uuid"))
    //   .repartition(16, col("uuid"))
    //   .write.parquet(path_user_profile_parquet_0)


    val user_profile_category_1 = sqlContext.read.parquet(path_user_profile_category_parquet)
      .withColumn("hash", hash_udf(col("uuid")))
      .filter("(hash % 2) = 1")
      .drop("hash")
      .withColumnRenamed("vector", "category_vector")

    val user_profile_entity_1 = sqlContext.read.parquet(path_user_profile_entity_parquet)
      .withColumn("hash", hash_udf(col("uuid")))
      .filter("(hash % 2) = 1")
      .drop("hash")
      .withColumnRenamed("vector", "entity_vector")

    val user_profile_topic_1 = sqlContext.read.parquet(path_user_profile_topic_parquet_1)
      .withColumnRenamed("vector", "topic_vector")

    user_profile_category_1
      .join(user_profile_topic_1, Seq("uuid"))
      .join(user_profile_entity_1, Seq("uuid"))
      .repartition(16, col("uuid"))
      .write.parquet(path_user_profile_parquet_1)
     
    sqlContext.read.parquet(path_user_profile_parquet_1).take(100).foreach{e:Row => println(e.mkString(" "))}


    sc.stop()
  }
}
