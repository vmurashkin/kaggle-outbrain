import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import scala.util.hashing.MurmurHash3
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.rdd.RDD


object Stats {

  case class Click(display_id: String, ad_id: String, clicked: Integer)
  case class PageView(uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String, traffic_source: Integer)
  case class Event(display_id: String, uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String)
  case class DocCategory(document_id: String, category_id: String, confidence_level: Double)
  case class DocEntity(document_id: String, entity_id: String, confidence_level: Double)
  case class DocMeta(document_id: String, source_id: String, publisher_id: String, publish_time: String)
  case class DocTopic(document_id: String, topic_id: String, confidence_level: Double)
  case class PromotedContent(ad_id: String, document_id: String, campaign_id: String, advertiser_id: String)


  def main(args: Array[String]) {

    var path_continent_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/continent_parquet"
    var path_clicks_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train_parquet"
    var path_clicks_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test_parquet"
    var path_events_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_parquet"
    var path_page_views_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views_parquet"
    var path_doc_categories_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories_parquet"
    var path_doc_entities_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities_parquet"
    var path_doc_meta_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta_parquet"
    var path_doc_topics_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics_parquet"
    var path_promoted_content_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content_parquet"

    var path_events_traffic_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_traffic_parquet"

    def hash_fun(arg: String): Int = MurmurHash3.stringHash(arg, 1883427247) & 0x7FFFFFFF
    val hash_udf = udf[Int, String](hash_fun)


    def geo_fun(arg: String, i: Integer): String = { val a = arg.split("[>]", -1) ++ Seq("", "", ""); a(i) }
    
    def geo_country(arg: String): String = geo_fun(arg, 0)
    def geo_state(arg: String): String = geo_fun(arg, 1)
    def geo_dma(arg: String): String = geo_fun(arg, 2)

    val geo_country_udf = udf[String, String](geo_country)
    val geo_state_udf = udf[String, String](geo_state)
    val geo_dma_udf = udf[String, String](geo_dma)


    def dow_fun(arg: Int): Int = ((arg / 1000) / 3600 / 24) % 7
    val dow_udf = udf[Int, Int](dow_fun)

    val conf = new SparkConf().setAppName("Stats")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    val continents = sqlContext.read.parquet(path_continent_parquet).cache

    val events = sqlContext.read.parquet(path_events_traffic_parquet)
      .withColumn("geo_country", geo_country_udf(col("geo_location")).as("geo_country"))
      .withColumn("geo_state", geo_state_udf(col("geo_location")).as("geo_state"))
      .withColumn("geo_dma", geo_dma_udf(col("geo_location")).as("geo_dma"))
      .withColumn("dow", dow_udf(col("timestamp")).as("dow"))
      .join(continents, Seq("geo_country"), "left_outer")

    val clicks = sqlContext.read.parquet(path_clicks_test_parquet)

    val click_events = clicks.join(events, Seq("display_id"))

    val promoted_content = sqlContext.read.parquet(path_promoted_content_parquet)
      .drop("document_id")
      .filter(col("campaign_id").notEqual(""))
      .distinct
      .cache

    val doc_meta = sqlContext.read.parquet(path_doc_meta_parquet)
      .filter(col("publisher_id").notEqual(""))
      

    // click_events.join(promoted_content, Seq("ad_id"))
    //   .groupBy("campaign_id")
    //   .agg(countDistinct("display_id").as("cnt"))
    //   .orderBy(desc("cnt"))
    //   .collect
    //   .foreach{r => println("campaign\t" + r.mkString("\t"))}

    // click_events.join(doc_meta, Seq("document_id"))
    //   .groupBy("publisher_id")
    //   .agg(countDistinct("display_id").as("cnt"))
    //   .orderBy(desc("cnt"))
    //   .collect
    //   .foreach{r => println("publisher\t" + r.mkString("\t"))}

    click_events
      .filter(col("geo_state").notEqual("") && col("geo_country").notEqual(""))
      .groupBy("geo_country", "geo_state")
      .agg(countDistinct("display_id").as("cnt"))
      .orderBy(desc("cnt"))
      .collect
      .foreach{r => println("geo\t" + r.mkString("\t"))}

    sc.stop()
  }
}
