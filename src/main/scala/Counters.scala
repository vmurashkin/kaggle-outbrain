import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import scala.util.hashing.MurmurHash3
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.rdd.RDD


object Counters {

  case class Click(display_id: String, ad_id: String, clicked: Integer)
  case class PageView(uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String, traffic_source: Integer)
  case class Event(display_id: String, uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String)
  case class DocCategory(document_id: String, category_id: String, confidence_level: Double)
  case class DocEntity(document_id: String, entity_id: String, confidence_level: Double)
  case class DocMeta(document_id: String, source_id: String, publisher_id: String, publish_time: String)
  case class DocTopic(document_id: String, topic_id: String, confidence_level: Double)
  case class PromotedContent(ad_id: String, document_id: String, campaign_id: String, advertiser_id: String)


  def main(args: Array[String]) {

    var path_continent_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/continent_parquet"
    var path_clicks_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train_parquet"
    var path_clicks_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test_parquet"
    var path_events_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_parquet"
    var path_page_views_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views_parquet"
    var path_doc_categories_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories_parquet"
    var path_doc_entities_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities_parquet"
    var path_doc_meta_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta_parquet"
    var path_doc_topics_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics_parquet"
    var path_promoted_content_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content_parquet"

    var path_events_traffic_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_traffic_parquet"

    // var path_ad_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/ad_ctr_parquet"
    // var path_doc_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/doc_ctr_parquet"
    // var path_platform_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/platform_ctr_parquet"
    // var path_traffic_source_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/traffic_source_ctr_parquet"
    // var path_geo_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/geo_ctr_parquet"
    // var path_dow_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/dow_ctr_parquet"
    // var path_category_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/category_ctr_parquet"
    // var path_continent_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/continent_ctr_parquet"
    // var path_country_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/country_ctr_parquet"
    // var path_state_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/state_ctr_parquet"
    // var path_dma_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/dma_ctr_parquet"
    // var path_publisher_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/publisher_ctr_parquet"
    // var path_source_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/source_ctr_parquet"
    // var path_camapign_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/camapign_ctr_parquet"
    // var path_advertiser_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/advertiser_ctr_parquet"

    var path_ad_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/ad_ctr_parquet"
    var path_doc_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/doc_ctr_parquet"
    var path_platform_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/platform_ctr_parquet"
    var path_traffic_source_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/traffic_source_ctr_parquet"
    var path_geo_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/geo_ctr_parquet"
    var path_dow_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/dow_ctr_parquet"
    var path_category_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/category_ctr_parquet"
    var path_continent_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/continent_ctr_parquet"
    var path_country_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/country_ctr_parquet"
    var path_state_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/state_ctr_parquet"
    var path_dma_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/dma_ctr_parquet"
    var path_publisher_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/publisher_ctr_parquet"
    var path_source_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/source_ctr_parquet"
    var path_camapign_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/camapign_ctr_parquet"
    var path_advertiser_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/test/advertiser_ctr_parquet"


    def hash_fun(arg: String): Int = MurmurHash3.stringHash(arg, 1883427247) & 0x7FFFFFFF
    val hash_udf = udf[Int, String](hash_fun)


    def geo_fun(arg: String, i: Integer): String = { val a = arg.split("[>]", -1) ++ Seq("", "", ""); a(i) }
    
    def geo_country(arg: String): String = geo_fun(arg, 0)
    def geo_state(arg: String): String = geo_fun(arg, 1)
    def geo_dma(arg: String): String = geo_fun(arg, 2)

    val geo_country_udf = udf[String, String](geo_country)
    val geo_state_udf = udf[String, String](geo_state)
    val geo_dma_udf = udf[String, String](geo_dma)


    def dow_fun(arg: Int): Int = ((arg / 1000) / 3600 / 24) % 7
    val dow_udf = udf[Int, Int](dow_fun)

    val conf = new SparkConf().setAppName("Counters")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    val continents = sqlContext.read.parquet(path_continent_parquet).cache

    val events = sqlContext.read.parquet(path_events_traffic_parquet)
      .withColumn("geo_country", geo_country_udf(col("geo_location")).as("geo_country"))
      .withColumn("geo_state", geo_state_udf(col("geo_location")).as("geo_state"))
      .withColumn("geo_dma", geo_dma_udf(col("geo_location")).as("geo_dma"))
      .withColumn("hash", hash_udf(col("uuid")).as("hash"))
      .withColumn("dow", dow_udf(col("timestamp")).as("dow"))
      .join(continents, Seq("geo_country"), "left_outer")
    // .filter(expr("(hash % 11 NOT IN (4, 3, 9))"))

    val clicks_train = sqlContext.read.parquet(path_clicks_train_parquet)
    val doc_category = sqlContext.read.parquet(path_doc_categories_parquet)

    val click_events = clicks_train.join(events, Seq("display_id"))

    val promoted_content = sqlContext.read.parquet(path_promoted_content_parquet)
      .drop("document_id")
      .distinct
      .cache

    // events.select("geo_location", "geo_country", "geo_state", "geo_dma", "dow").take(1000).foreach{e:Row => println(e.mkString("\t"))}

    // sqlContext.read.parquet(path_dow_ctr_parquet).take(1000).foreach{e:Row => println(e.mkString("\t"))}

    val doc_categories = sqlContext.read.parquet(path_doc_categories_parquet)

    click_events
      .join(doc_categories, Seq("document_id"))
      .groupBy("ad_id", "category_id")
      .agg(sum(expr("clicked * confidence_level")).as("category_clicks"),
        sum("confidence_level").as("category_impressions"))
      .filter(col("category_impressions").geq(10))
      .repartition(16, col("ad_id"))
      .write.parquet(path_category_ctr_parquet)

    click_events.groupBy("ad_id", "dow")
      .agg(sum("clicked").as("dow_clicks"),
        count("ad_id").as("dow_impressions"))
      .filter(col("dow_impressions").geq(10))
      .repartition(8, col("ad_id"))
      .write.parquet(path_dow_ctr_parquet)

    // val continent_ctr = 
    click_events
      .filter(col("geo_country").isNotNull)
      .groupBy("geo_continent", "ad_id")
      .agg(sum("clicked").as("continent_clicks"),
        count("ad_id").as("continent_impressions")) 
      .filter(col("continent_impressions").geq(10))
      .repartition(8, col("ad_id"))
      .write.parquet(path_continent_ctr_parquet)

    // val country_ctr =
    click_events
      .filter(col("geo_country").notEqual("\\N") && col("geo_country").notEqual("--"))
      .groupBy("geo_continent", "geo_country", "ad_id")
      .agg(sum("clicked").as("country_clicks"),
        count("ad_id").as("country_impressions")) 
      .filter(col("country_impressions").geq(10))
      .repartition(8, col("ad_id"))
      .write.parquet(path_country_ctr_parquet)

    // val state_ctr =
    click_events
      .filter(col("geo_country").notEqual("\\N") && col("geo_country").notEqual("--"))
      .filter(col("geo_state").notEqual(""))
      .groupBy("geo_continent", "geo_country", "geo_state", "ad_id")
      .agg(sum("clicked").as("state_clicks"),
        count("ad_id").as("state_impressions")) 
      .filter(col("state_impressions").geq(10))
      .repartition(8, col("ad_id"))
      .write.parquet(path_state_ctr_parquet)

    // val dma_ctr =
    click_events
      .filter(col("geo_country").notEqual("\\N") && col("geo_country").notEqual("--"))
      .filter(col("geo_state").notEqual(""))
      .filter(col("geo_dma").notEqual(""))
      .groupBy("geo_continent", "geo_country", "geo_state", "geo_dma", "ad_id")
      .agg(sum("clicked").as("dma_clicks"),
        count("ad_id").as("dma_impressions")) 
      .filter(col("dma_impressions").geq(10))
      .repartition(8, col("ad_id"))
      .write.parquet(path_dma_ctr_parquet)

    click_events
      .filter(col("platform").isin(Seq(1, 2, 3) : _*))
      .groupBy("ad_id", "platform")
      .agg(sum("clicked").as("platform_clicks"), count("ad_id").as("platform_impressions"))
      .filter(col("platform_impressions").geq(10))
      .repartition(4, col("ad_id"))
      .write.parquet(path_platform_ctr_parquet)

    click_events
      .filter(col("traffic_source").isin(Seq(1, 2, 3) : _*))
      .groupBy("ad_id", "traffic_source")
      .agg(sum("clicked").as("traffic_source_clicks"), count("ad_id").as("traffic_source_impressions"))
      .filter(col("traffic_source_impressions").geq(10))
      .repartition(4, col("ad_id"))
      .write.parquet(path_traffic_source_ctr_parquet)

    val doc_meta = sqlContext.read.parquet(path_doc_meta_parquet)

    val click_events_meta = click_events.join(doc_meta, Seq("document_id"))

    // val publisher_ctr =
    click_events_meta
      .filter(col("publisher_id").notEqual(""))
      .groupBy("publisher_id", "ad_id")
      .agg(sum("clicked").as("publisher_clicks"),
        count("ad_id").as("publisher_impressions")) 
      .filter(col("publisher_impressions").geq(10))
      .repartition(4, col("ad_id"))
      .write.parquet(path_publisher_ctr_parquet)

    // val source_ctr =
    click_events_meta
      .filter(col("publisher_id").notEqual(""))
      .filter(col("source_id").notEqual(""))
      .groupBy("publisher_id", "source_id", "ad_id")
      .agg(sum("clicked").as("source_clicks"),
        count("ad_id").as("source_impressions"))
      .filter(col("source_impressions").geq(10))
      .repartition(4, col("ad_id"))
      .write.parquet(path_source_ctr_parquet)

    // val doc_ctr = 
    click_events
      .groupBy("document_id", "ad_id")
      .agg(sum("clicked").as("doc_clicks"),
        count("ad_id").as("doc_impressions")) 
      .filter(col("doc_impressions").geq(10))
      .repartition(16, col("ad_id"))
      .write.parquet(path_doc_ctr_parquet)

    val click_events_promoted = click_events.join(promoted_content, Seq("ad_id"))

    // val ad_ctr = 
    click_events_promoted
      .groupBy("advertiser_id", "campaign_id", "ad_id")
      .agg(sum("clicked").as("ad_clicks"),
        count("ad_id").as("ad_impressions")) 
      .filter(col("ad_impressions").geq(10))
      .repartition(16, col("ad_id"))
      .write.parquet(path_ad_ctr_parquet)

    val campaign_ctr = click_events_promoted
      .filter(col("advertiser_id").notEqual(""))
      .filter(col("campaign_id").notEqual(""))
      .groupBy("advertiser_id", "campaign_id")
      .agg(sum("clicked").as("campaign_clicks"),
        count("campaign_id").as("campaign_impressions")) 
      .filter(col("campaign_impressions").geq(10))
      .repartition(4, col("advertiser_id"))
      .write.parquet(path_camapign_ctr_parquet)

    // val advertiser_ctr = click_events
    click_events_promoted
      .filter(col("advertiser_id").notEqual(""))
      .groupBy("advertiser_id")
      .agg(sum("clicked").as("advertiser_clicks"),
        count("advertiser_id").as("advertiser_impressions"))
      .filter(col("advertiser_impressions").geq(10))
      .repartition(4, col("advertiser_id"))
      .write.parquet(path_advertiser_ctr_parquet)

    sc.stop()
  }
}
