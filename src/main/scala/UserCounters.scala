import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import scala.util.hashing.MurmurHash3
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.rdd.RDD


object UserCounters {

  case class Click(display_id: String, ad_id: String, clicked: Integer)
  case class PageView(uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String, traffic_source: Integer)
  case class Event(display_id: String, uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String)
  case class DocCategory(document_id: String, category_id: String, confidence_level: Double)
  case class DocEntity(document_id: String, entity_id: String, confidence_level: Double)
  case class DocMeta(document_id: String, source_id: String, publisher_id: String, publish_time: String)
  case class DocTopic(document_id: String, topic_id: String, confidence_level: Double)
  case class PromotedContent(ad_id: String, document_id: String, campaign_id: String, advertiser_id: String)


  def main(args: Array[String]) {

    var path_continent_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/continent_parquet"
    var path_clicks_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train_parquet"
    var path_clicks_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test_parquet"
    var path_events_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_parquet"
    var path_page_views_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views_parquet"
    var path_doc_categories_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories_parquet"
    var path_doc_entities_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities_parquet"
    var path_doc_meta_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta_parquet"
    var path_doc_topics_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics_parquet"
    var path_promoted_content_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content_parquet"

    var path_user_seen_ad_ts = "hdfs:///user/vmurashkin/kaggle/outbrain/user_seen_ad_ts"
    var path_user_seen_campaign_ts = "hdfs:///user/vmurashkin/kaggle/outbrain/user_seen_campaign_ts"
    var path_user_click_ad_ts = "hdfs:///user/vmurashkin/kaggle/outbrain/user_click_ad_ts"
    var path_user_click_campaign_ts = "hdfs:///user/vmurashkin/kaggle/outbrain/user_click_campaign_ts"

    val conf = new SparkConf().setAppName("UserCounters")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    val promoted_content = sqlContext.read.parquet(path_promoted_content_parquet)
    val page_views = sqlContext.read.parquet(path_page_views_parquet)

    val events = sqlContext.read.parquet(path_events_parquet)

    val clicks_train = sqlContext.read.parquet(path_clicks_train_parquet)
    val clicks_test = sqlContext.read.parquet(path_clicks_test_parquet)

    val clicks = clicks_train.drop("clicked").unionAll(clicks_test.drop("clicked"))

    val click_events = clicks.join(events, Seq("display_id"))

    
    val page_view_promoted = page_views
      .select("uuid", "document_id", "timestamp")
      .join(promoted_content, Seq("document_id"))
    
    page_view_promoted
      .groupBy("uuid", "ad_id")
      .agg(min(col("timestamp")).as("min_ad_timestamp"),
         max(col("timestamp")).as("max_ad_timestamp"))
      .repartition(16, col("ad_id"))
      .write.parquet(path_user_seen_ad_ts)

    page_view_promoted
      .groupBy("uuid", "campaign_id")
      .agg(min(col("timestamp")).as("min_campaign_timestamp"),
         max(col("timestamp")).as("max_campaign_timestamp"))
      .filter(col("campaign_id").notEqual(""))
      .repartition(8, col("campaign_id"))
      .write.parquet(path_user_seen_campaign_ts)

    val promoted_clicks = click_events
      .join(promoted_content.drop("document_id").distinct, Seq("ad_id"))
 
    promoted_clicks.groupBy("uuid", "ad_id")
      .agg(min(col("timestamp")).as("min_ad_timestamp"),
         max(col("timestamp")).as("max_ad_timestamp"))
      .repartition(16, col("ad_id"))
      .write.parquet(path_user_click_ad_ts)
    
    promoted_clicks.groupBy("uuid", "campaign_id")
      .agg(min(col("timestamp")).as("min_campaign_timestamp"),
         max(col("timestamp")).as("max_campaign_timestamp"))
      .filter(col("campaign_id").notEqual(""))
      .repartition(8, col("campaign_id"))
      .write.parquet(path_user_click_campaign_ts)

    sc.stop()
  }
}
