import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import scala.util.hashing.MurmurHash3
import scala.collection.mutable.WrappedArray
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.Matrix
import org.apache.spark.mllib.linalg.distributed.IndexedRow
import org.apache.spark.mllib.linalg.distributed.IndexedRowMatrix
import org.apache.spark.mllib.linalg.SingularValueDecomposition
import org.apache.spark.mllib.linalg.Vectors._

object UserProfileSVD {


  def main(args: Array[String]) {


    var path_user_profile_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_parquet"


    var path_user_profile_svd = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_svd_parquet"

    def hash_fun(arg: String): Int = MurmurHash3.stringHash(arg, 1883427247) & 0x7FFFFFFF
    val hash_udf = udf[Int, String](hash_fun)


    val conf = new SparkConf().setAppName("UserProfile")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    val svd_type = new StructType()
        .add("unique_id", LongType)
        .add("vector", ArrayType(DoubleType))



    val user_profile = sqlContext.read
      .parquet(path_user_profile_parquet)
      .withColumn("unique_id", monotonicallyIncreasingId)

    val user_profile_topic = user_profile.select("unique_id", "uuid", "topic_vector")
    val mat_topic = new IndexedRowMatrix(user_profile_topic.map(r => IndexedRow(r.getLong(0), dense(r.getAs[Seq[Double]](2).toArray))))
    val svd_topic_u = mat_topic.computeSVD(8, computeU=true).U
    val svd_topic_rows = svd_topic_u.rows.map(r => Row(r.index, r.vector.toArray.map(_ / norm(r.vector, 2d))))
    val svd_topic_df = sqlContext.createDataFrame(svd_topic_rows, svd_type)

    val user_profile_entity = user_profile.select("unique_id", "uuid", "entity_vector")
    val mat_entity = new IndexedRowMatrix(user_profile_entity.map(r => IndexedRow(r.getLong(0), dense(r.getAs[Seq[Double]](2).toArray))))
    val svd_entity_u = mat_entity.computeSVD(8, computeU=true).U
    val svd_entity_rows = svd_entity_u.rows.map(r => Row(r.index, r.vector.toArray.map(_ / norm(r.vector, 2d))))
    val svd_entity_df = sqlContext.createDataFrame(svd_entity_rows, svd_type)

    val user_profile_category = user_profile.select("unique_id", "uuid", "category_vector")
    val mat_category = new IndexedRowMatrix(user_profile_category.map(r => IndexedRow(r.getLong(0), dense(r.getAs[Seq[Double]](2).toArray))))
    val svd_category_u = mat_category.computeSVD(8, computeU=true).U
    val svd_category_rows = svd_category_u.rows.map(r => Row(r.index, r.vector.toArray.map(_ / norm(r.vector, 2d))))
    val svd_category_df = sqlContext.createDataFrame(svd_category_rows, svd_type)



    user_profile
      .drop("topic_vector")
      .drop("category_vector")
      .drop("entity_vector")
      .join(svd_topic_df, Seq("unique_id"))
      .withColumnRenamed("vector", "topic_vector")
      .join(svd_entity_df, Seq("unique_id"))
      .withColumnRenamed("vector", "entity_vector")
      .join(svd_category_df, Seq("unique_id"))
      .withColumnRenamed("vector", "category_vector")
      .write.parquet(path_user_profile_svd)

    sqlContext.read.parquet(path_user_profile_svd).take(1000).foreach{e:Row => println(e.mkString(" "))}

    sc.stop()
  }
}
