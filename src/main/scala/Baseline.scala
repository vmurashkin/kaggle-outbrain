import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._


object Baseline {

  case class Click(display_id: String, ad_id: String, clicked: Integer)
  case class PageView(uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String, traffic_source: Integer)
  case class Event(display_id: String, uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String)
  case class DocCategory(document_id: String, category_id: String, confidence_level: Double)
  case class DocEntity(document_id: String, entity_id: String, confidence_level: Double)
  case class DocMeta(document_id: String, source_id: String, publisher_id: String, publish_time: String)
  case class DocTopic(document_id: String, topic_id: String, confidence_level: Double)
  case class PromotedContent(ad_id: String, document_id: String, campaign_id: String, advertiser_id: String)


  object orderByProb extends UserDefinedAggregateFunction {
      // Schema you get as an input
      def inputSchema = new StructType()
        .add("ad_id", StringType)
        .add("prob", DoubleType)

      // Schema of the row which is used for aggregation
      def bufferSchema = new StructType()
        .add("ad_id", ArrayType(StringType))
        .add("prob", ArrayType(DoubleType))

      // Returned type
      def dataType = StringType
      // Self-explaining 
      def deterministic = true
      // zero value
      def initialize(buffer: MutableAggregationBuffer) = {
        buffer.update(0, Array.empty[String])
        buffer.update(1, Array.empty[Double])
      }
      // Similar to seqOp in aggregate
      def update(buffer: MutableAggregationBuffer, input: Row) = {
          if (!input.isNullAt(0) && !input.isNullAt(1))
            buffer.update(0, buffer.getAs[Seq[String]](0) :+ input.getString(0))
            buffer.update(1, buffer.getAs[Seq[Double]](1) :+ input.getDouble(1))
      }
      // Similar to combOp in aggregate
      def merge(buffer1: MutableAggregationBuffer, buffer2: Row) = {
        buffer1.update(0, buffer1.getAs[Seq[String]](0) ++ buffer2.getAs[Seq[String]](0))    
        buffer1.update(1, buffer1.getAs[Seq[Double]](1) ++ buffer2.getAs[Seq[Double]](1))    
      }
      // Called on exit to get return value
      def evaluate(buffer: Row) = {
        buffer.getAs[Seq[String]](0)
          .zip(buffer.getAs[Seq[Double]](1))
          .sortBy{ case (ad_id, prob) => (-prob, ad_id) }
          .map(_._1)
          .take(12)
          .mkString(" ")
      }
  }


  def main(args: Array[String]) {

    var path_clicks_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train_parquet"
    var path_clicks_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test_parquet"
    var path_events_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_parquet"
    var path_page_views_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views_parquet"
    var path_doc_categories_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories_parquet"
    var path_doc_entities_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities_parquet"
    var path_doc_meta_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta_parquet"
    var path_doc_topics_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics_parquet"
    var path_promoted_content_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content_parquet"

    var path_ad_doc_ctr_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/ad_doc_ctr_parquet"

    val conf = new SparkConf().setAppName("Baseline")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    val events = sqlContext.read.parquet(path_events_parquet)
    val clicks_test = sqlContext.read.parquet(path_clicks_test_parquet)
    val ad_doc_ctr = sqlContext.read.parquet(path_ad_doc_ctr_parquet)

    val baseline = clicks_test.join(events, Seq("display_id"), "inner")
      .select("display_id", "ad_id", "document_id")
      .join(ad_doc_ctr, Seq("ad_id", "document_id"), "left_outer")
      .selectExpr("display_id", "ad_id", "(COALESCE(clicks, 0) + 10) / (COALESCE(impressions, 0) + 1000) AS prob")
      .groupBy("display_id")
      .agg(orderByProb($"ad_id", $"prob"))

    baseline.collect.foreach{e: Row => println(e.mkString(","))}

    sc.stop()
  }
}
