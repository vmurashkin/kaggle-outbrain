import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import scala.util.hashing.MurmurHash3
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import org.apache.spark.rdd.RDD


object Lookalike {

  case class Click(display_id: String, ad_id: String, clicked: Integer)
  case class PageView(uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String, traffic_source: Integer)
  case class Event(display_id: String, uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String)
  case class DocCategory(document_id: String, category_id: String, confidence_level: Double)
  case class DocEntity(document_id: String, entity_id: String, confidence_level: Double)
  case class DocMeta(document_id: String, source_id: String, publisher_id: String, publish_time: String)
  case class DocTopic(document_id: String, topic_id: String, confidence_level: Double)
  case class PromotedContent(ad_id: String, document_id: String, campaign_id: String, advertiser_id: String)

  object avgVectorUDF extends UserDefinedAggregateFunction {
      // Schema you get as an input
      def inputSchema = new StructType()
        .add("vector", ArrayType(DoubleType))

      // Schema of the row which is used for aggregation
      def bufferSchema = new StructType()
        .add("count", DoubleType)
        .add("accumulator", ArrayType(DoubleType))

      // Returned type
      def dataType = ArrayType(DoubleType)
      // Self-explaining
      def deterministic = true
      // zero value
      def initialize(buffer: MutableAggregationBuffer) = {
        buffer.update(0, 0d)
        buffer.update(1, List.fill(8)(0d).toArray)
      }
      // Similar to seqOp in aggregate
      def update(buffer: MutableAggregationBuffer, input: Row) = {
          if (!input.isNullAt(0)) {
            buffer.update(0, buffer.getDouble(0) + 1d)
            buffer.update(1, buffer.getAs[Seq[Double]](1).zip(input.getAs[Seq[Double]](0)).map(x => x._1 + x._2))
          }
      }
      // Similar to combOp in aggregate
      def merge(buffer1: MutableAggregationBuffer, buffer2: Row) = {
        buffer1.update(0, buffer1.getDouble(0) + buffer2.getDouble(0))
        buffer1.update(1, buffer1.getAs[Seq[Double]](1).zip(buffer2.getAs[Seq[Double]](1)).map(x => x._1 + x._2))
      }

      // Called on exit to get return value
      def evaluate(buffer: Row) = {
        val count = buffer.getDouble(0)
        val vector = buffer.getAs[Seq[Double]](1).map(_ / count).toArray
        val norm = math.pow(vector.map(math.pow(_, 2d)).foldLeft(0d)(_+_), 0.5d)
        vector.map(_ / norm).toArray
      }
  }


  def main(args: Array[String]) {

    var path_continent_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/continent_parquet"
    var path_clicks_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train_parquet"
    var path_clicks_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test_parquet"
    var path_events_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_parquet"
    var path_page_views_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views_parquet"
    var path_doc_categories_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories_parquet"
    var path_doc_entities_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities_parquet"
    var path_doc_meta_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta_parquet"
    var path_doc_topics_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics_parquet"
    var path_promoted_content_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content_parquet"

    var path_events_traffic_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_traffic_parquet"

    var path_user_profile_svd = "hdfs:///user/vmurashkin/kaggle/outbrain/67/user_profile_svd_parquet"

    // var path_ad_center = "hdfs:///user/vmurashkin/kaggle/outbrain/67/ad_center_parquet"
    // var path_campaign_center = "hdfs:///user/vmurashkin/kaggle/outbrain/67/campaign_center_parquet"

    var path_ad_center = "hdfs:///user/vmurashkin/kaggle/outbrain/67/test/ad_center_parquet"
    var path_campaign_center = "hdfs:///user/vmurashkin/kaggle/outbrain/67/test/campaign_center_parquet"

    def hash_fun(arg: String): Int = MurmurHash3.stringHash(arg, 1883427247) & 0x7FFFFFFF
    val hash_udf = udf[Int, String](hash_fun)


    val conf = new SparkConf().setAppName("UserProfile")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    val clicks = sqlContext.read.parquet(path_clicks_train_parquet)
    val events = sqlContext.read.parquet(path_events_parquet)
    val promoted_content = sqlContext.read.parquet(path_promoted_content_parquet).drop("document_id").distinct.cache

    val user_profile_svd = 
      sqlContext.read.parquet(path_user_profile_svd)
      //.withColumn("hash", hash_udf(col("uuid")).as("hash"))
      //.filter(expr("(hash % 11 NOT IN (4, 3, 9))"))

    val user_ad_clicks = events
      .join(clicks, Seq("display_id"))
      .groupBy("uuid", "ad_id")
      .agg(max(col("clicked")).as("clicked"))

    val user_campaign_clicks_profile = user_ad_clicks
      .join(promoted_content, Seq("ad_id"))
      .groupBy("uuid", "campaign_id")
      .agg(max(col("clicked")).as("clicked"))
      .join(user_profile_svd, Seq("uuid"))

    val user_ad_clicks_profile = user_ad_clicks
      .join(user_profile_svd, Seq("uuid"))


    val user_ad_clicked =
      user_ad_clicks_profile.filter("clicked = 1")
      .groupBy("ad_id")
      .agg(avgVectorUDF(col("category_vector")).as("ad_category_center_clicked"),
        avgVectorUDF(col("topic_vector")).as("ad_topic_center_clicked"),
        avgVectorUDF(col("entity_vector")).as("ad_entity_center_clicked"))

    val user_ad_nonclicked =
      user_ad_clicks_profile.filter("clicked = 0")
      .groupBy("ad_id")
      .agg(avgVectorUDF(col("category_vector")).as("ad_category_center_nonclicked"),
        avgVectorUDF(col("topic_vector")).as("ad_topic_center_nonclicked"),
        avgVectorUDF(col("entity_vector")).as("ad_entity_center_nonclicked"))

    user_ad_clicked
      .join(user_ad_nonclicked, Seq("ad_id"))
      .repartition(4, col("ad_id"))
      .write.parquet(path_ad_center)

    sqlContext.read.parquet(path_ad_center).take(100).foreach{e:Row => println("ad: " + e.mkString(" "))}


    val user_campaign_clicked =
      user_campaign_clicks_profile.filter("clicked = 1")
      .groupBy("campaign_id")
      .agg(avgVectorUDF(col("category_vector")).as("campaign_category_center_clicked"),
        avgVectorUDF(col("topic_vector")).as("campaign_topic_center_clicked"),
        avgVectorUDF(col("entity_vector")).as("campaign_entity_center_clicked"))

    val user_campaign_nonclicked =
      user_campaign_clicks_profile.filter("clicked = 0")
      .groupBy("campaign_id")
      .agg(avgVectorUDF(col("category_vector")).as("campaign_category_center_nonclicked"),
        avgVectorUDF(col("topic_vector")).as("campaign_topic_center_nonclicked"),
        avgVectorUDF(col("entity_vector")).as("campaign_entity_center_nonclicked"))

    user_campaign_clicked
      .join(user_campaign_nonclicked, Seq("campaign_id"))
      .repartition(4, col("campaign_id"))
      .write.parquet(path_campaign_center)

    sqlContext.read.parquet(path_campaign_center).take(100).foreach{e:Row => println("camp: " + e.mkString(" "))}

    sc.stop()
  }
}
