import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.Date
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.text.SimpleDateFormat
import org.apache.spark.input.PortableDataStream
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.commons.lang.math.NumberUtils


// http://spark.apache.org/docs/latest/sql-programming-guide.html#programmatically-specifying-the-schema

object Csv2Parquet {

  case class Click(display_id: String, ad_id: String, clicked: Integer)
  case class Continent(geo_country: String, geo_continent: String)
  case class PageView(uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String, traffic_source: Integer)
  case class Event(display_id: String, uuid: String, document_id: String, timestamp: Integer, platform: Integer, geo_location: String)
  case class DocCategory(document_id: String, category_id: String, confidence_level: Double)
  case class DocEntity(document_id: String, entity_id: String, confidence_level: Double)
  case class DocMeta(document_id: String, source_id: String, publisher_id: String, publish_time: String)
  case class DocTopic(document_id: String, topic_id: String, confidence_level: Double)
  case class PromotedContent(ad_id: String, document_id: String, campaign_id: String, advertiser_id: String)


  def readCsvZip(sc: SparkContext, path: String, skip_lines: Integer = 0) : RDD[Array[String]] = {
     sc.binaryFiles(path).flatMap { case (name: String, content: PortableDataStream) =>
      val zis = new ZipInputStream(content.open())
      Stream.continually(zis.getNextEntry)
        .takeWhile(_ != null)
        .filter(!_.isDirectory)
        .flatMap { e: ZipEntry =>
          val br = new BufferedReader(new InputStreamReader(zis, "UTF-8"))
          Stream.continually(br.readLine)
            .drop(skip_lines)
            .takeWhile(_ != null)
            .filter(_ != "")
            .map(_.split(",", -1))
      }
    }
  }

// http://dev.maxmind.com/geoip/legacy/codes/country_continent/

  def main(args: Array[String]) {

    var path_clicks_train = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train.csv.zip"
    var path_clicks_test = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test.csv.zip"
    var path_events = "hdfs:///user/vmurashkin/kaggle/outbrain/events.csv.zip"
    var path_page_views = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views.csv.zip"
    var path_doc_categories = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories.csv.zip"
    var path_doc_entities = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities.csv.zip"
    var path_doc_meta = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta.csv.zip"
    var path_doc_topics = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics.csv.zip"
    var path_promoted_content = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content.csv.zip"
    var path_continent = "hdfs:///user/vmurashkin/kaggle/outbrain/country_continent.csv.zip"

    var path_clicks_train_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_train_parquet"
    var path_clicks_test_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/clicks_test_parquet"
    var path_events_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/events_parquet"
    var path_page_views_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/page_views_parquet"
    var path_doc_categories_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_categories_parquet"
    var path_doc_entities_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_entities_parquet"
    var path_doc_meta_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_meta_parquet"
    var path_doc_topics_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/documents_topics_parquet"
    var path_promoted_content_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/promoted_content_parquet"
    var path_continent_parquet = "hdfs:///user/vmurashkin/kaggle/outbrain/path_continent_parquet"

    var formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    val conf = new SparkConf().setAppName("Csv2Parquet")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)  // Note this is *deprecated* in 2.0.0
    import sqlContext.implicits._

    // val rdd = readCsvZip(sc, path_promoted_content)
    // rdd.take(50).foreach{ case e: Array[String] => println(e.mkString("\t")) }

    // readCsvZip(sc, path_continent, 1)
    //    .map(e => Continent(e(0), e(1)))
    //    .repartition(1)
    //    .toDF
    //    .write
    //    .parquet(path_continent_parquet)

    // sqlContext.read.parquet(path_continent_parquet).take(100).foreach{ case e: Row => println(e.mkString("\t")) }

    // readCsvZip(sc, path_promoted_content, 1)
    //   .map(e => PromotedContent(e(0), e(1), e(2), e(3)))
    //   .toDF
    //   .write
    //   .parquet(path_promoted_content_parquet)

    // readCsvZip(sc, path_doc_topics, 1)
    //   .map(e => DocTopic(e(0), e(1), e(2).toDouble))
    //   .toDF
    //   .write
    //   .parquet(path_doc_topics_parquet)

    // readCsvZip(sc, path_doc_meta, 1)
    //   .map(e => DocMeta(e(0), e(1), e(2), e(3)))
    //   .toDF
    //   .write
    //   .parquet(path_doc_meta_parquet)

    // val rdd = readCsvZip(sc, path_doc_categories)
    //   .map(e => DocCategory(e(0), e(1), e(2).toDouble))
    //   .toDF
    //   .write
    //   .parquet(path_doc_categories_parquet)

    // val rdd = readCsvZip(sc, path_clicks_train)
    //   .map(e => Click(e(0), e(1), e(2).toInt))
    //   .repartition(16)
    //   .toDF
    //   .write
    //   .parquet(path_clicks_train_parquet)

    // val rdd = readCsvZip(sc, path_clicks_test)
    //   .map(e => Click(e(0), e(1), 0))
    //   .repartition(8)
    //   .toDF
    //   .write
    //   .parquet(path_clicks_test_parquet)

    // val rdd = readCsvZip(sc, path_page_views)
    //   .repartition(128)
    //   .map(e => PageView(e(0), e(1), e(2).toInt, NumberUtils.toInt(e(3), 0), e(4), if (e.length > 5) NumberUtils.toInt(e(5), 0) else 0))
    //   .toDF
    //   .write
    //   .parquet(path_page_views_parquet)

    // val rdd_events = readCsvZip(sc, path_events)
    //   .map(e => Event(e(0), e(1), e(2), e(3).toInt, NumberUtils.toInt(e(4), 0), if (e.length > 5) e(5) else "\\N"))
    //   .repartition(16)
    //   .toDF
    //   .write
    //   .parquet(path_events_parquet)
    
    sc.stop()
  }
}
