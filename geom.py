import sys
import math

if __name__ == '__main__':

    for l in sys.stdin:
        v = map(float, l.split())
        p = v[0]
        for e in v[1:]: p = p * e
        print math.pow(p, 1. / len(v))
  
