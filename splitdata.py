import sys
import mmh3
from itertools import ifilter

def parse_rows(lines):
    for l in lines:
        v = l.rstrip().split(',')
        display_id, ad_id, uuid_hash, clicked = v[:4] 
        yield display_id, ad_id, uuid_hash, int(clicked), map(float, v[4:])


def check_split((display_id, ad_id, uuid_hash, clicked, v), mod=1, buckets=None):
    buckets = buckets or {0}
    return ((mmh3.hash('%s_a12zge42ed8' % uuid_hash) & 0x7fffffff) % mod) in buckets 


def split_rows(lines, mod=1, buckets=None):
    _check_split = lambda row: check_split(row, mod, buckets or {0})
    for row in ifilter(_check_split, parse_rows(lines)):
        yield row


if __name__ == '__main__':

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--mod', type=int, default=7)
    parser.add_argument('--buckets', nargs='+', type=int, default=[1, 4]) 

    args = parser.parse_args()
    print >> sys.stderr, args

    for row in split_rows(sys.stdin, args.mod, args.buckets):
        print ','.join(map(str, list(row[:4]) + row[4]))
