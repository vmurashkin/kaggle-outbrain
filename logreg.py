import sys
import mmh3
import numpy as np
from array import array
from operator import itemgetter
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble.bagging import _parallel_predict_proba
from sklearn.utils.validation import has_fit_parameter, check_is_fitted
from sklearn.utils import check_random_state, check_X_y, check_array, column_or_1d
from sklearn.model_selection import cross_val_score
from sklearn.ensemble.base import BaseEnsemble, _partition_estimators
from sklearn.externals.joblib import Parallel, delayed
from splitdata import split_rows


class BaggingClassifierGeom(BaggingClassifier):

    def __init__(self,
                 base_estimator=None,
                 n_estimators=10,
                 max_samples=1.0,
                 max_features=1.0,
                 bootstrap=True,
                 bootstrap_features=False,
                 oob_score=False,
                 warm_start=False,
                 n_jobs=1,
                 random_state=None,
                 verbose=0):

        super(BaggingClassifierGeom, self).__init__(
            base_estimator,
            n_estimators=n_estimators,
            max_samples=max_samples,
            max_features=max_features,
            bootstrap=bootstrap,
            bootstrap_features=bootstrap_features,
            oob_score=oob_score,
            warm_start=warm_start,
            n_jobs=n_jobs,
            random_state=random_state,
            verbose=verbose)


    def predict_proba(self, X):
        """Predict class probabilities for X.
        The predicted class probabilities of an input sample is computed as
        the mean predicted class probabilities of the base estimators in the
        ensemble. If base estimators do not implement a ``predict_proba``
        method, then it resorts to voting and the predicted class probabilities
        of an input sample represents the proportion of estimators predicting
        each class.
        Parameters
        ----------
        X : {array-like, sparse matrix} of shape = [n_samples, n_features]
            The training input samples. Sparse matrices are accepted only if
            they are supported by the base estimator.
        Returns
        -------
        p : array of shape = [n_samples, n_classes]
            The class probabilities of the input samples. The order of the
            classes corresponds to that in the attribute `classes_`.
        """
        check_is_fitted(self, "classes_")
        # Check data
        X = check_array(X, accept_sparse=['csr', 'csc'])

        if self.n_features_ != X.shape[1]:
            raise ValueError("Number of features of the model must "
                             "match the input. Model n_features is {0} and "
                             "input n_features is {1}."
                             "".format(self.n_features_, X.shape[1]))

        # Parallel loop
        n_jobs, n_estimators, starts = _partition_estimators(self.n_estimators,
                                                             self.n_jobs)

        all_proba = Parallel(n_jobs=n_jobs, verbose=self.verbose)(
            delayed(_parallel_predict_proba)(
                self.estimators_[starts[i]:starts[i + 1]],
                self.estimators_features_[starts[i]:starts[i + 1]],
                X,
                self.n_classes_)
            for i in range(n_jobs))

        proba = all_proba[0]

        for e in all_proba[1:]:
            np.multiply(proba, e, proba)

        np.power(proba, 1. / len(all_proba), proba)
        return proba

        # Reduce
        # proba = sum(all_proba) / self.n_estimators

        # return proba

def load_data(lines, n_features, mod=1, buckets=None):
    X, y = [], []
    for display_id, ad_id, uuid_hash, clicked, x in split_rows(lines, mod, buckets):
        X.append(array('f', map(abs, x[:n_features])))
        y.append(clicked)
    return np.array(X), np.array(y)


def predict(lines, clf, n_features, buffer_size=2500000):

    buf = []
    while True:
        del buf[:]
        try: buf.extend(lines.next() for _ in xrange(buffer_size))
        except StopIteration: pass
        if not buf: break
        X, _ = load_data(buf, n_features)
        for p in clf.predict_proba(np.array(X)):
            yield p


if __name__ == '__main__':

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--mod', type=int, default=7)
    parser.add_argument('--buckets', nargs='+', type=int, default=[0, 6]) 
    parser.add_argument('--n_features', type=int, default=34)
    parser.add_argument('--bagging', type=int, default=10)
    parser.add_argument('--random_state', type=int, default=12345)
    parser.add_argument('--penalty', default='l2')
    parser.add_argument('--n_jobs', default=4, type=int)
    parser.add_argument('--predict')

    args = parser.parse_args()
    print >> sys.stderr, args

    X_train, y_train = load_data(sys.stdin, args.n_features, args.mod, set(args.buckets))

    print >> sys.stderr, 'data loaded: %d' % len(X_train)

    clf = LogisticRegression(class_weight='balanced',
      penalty=args.penalty,
      random_state=args.random_state,
      max_iter=250)

    if args.bagging > 1:
        clf = BaggingClassifierGeom(base_estimator=clf,
                                    n_estimators=args.bagging,
                                    max_samples=0.75,
                                    n_jobs=args.n_jobs)

    if not args.predict:
        scores = cross_val_score(clf, X_train, y_train, n_jobs=args.n_jobs, cv=5, scoring='neg_log_loss') 
        print >> sys.stderr, scores.mean(), scores.std() * 2
        exit(0)

    clf.fit(X_train, y_train)
    print >> sys.stderr, 'model fitted'

    with open(args.predict) as src:
        for p in predict(src, clf, args.n_features):
            print p[1]
