spark-submit --class Baseline --master yarn --deploy-mode client --driver-memory 16g --conf spark.driver.maxResultSize=4g ./target/scala-2.10/dummy_2.10-1.0.jar > tmp
spark-submit --packages com.databricks:spark-csv_2.10:1.5.0 --class Dummy --verbose --driver-memory 8g --executor-memory 8g --conf spark.driver.maxResultSize=8g ./target/scala-2.10/dummy_2.10-1.0.jar
