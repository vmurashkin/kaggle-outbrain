import sys
import mmh3
from operator import itemgetter
from collections import Counter
from itertools import groupby


if __name__ == '__main__':

    counter_pos = Counter()
    counter_total = Counter()
    lines = iter(l.rstrip().split(',') for l in sys.stdin)

    for k, v in groupby(lines, itemgetter(0)):

        if ((mmh3.hash('%s_asdq45fq' % k) & 0x7fffffff) % 3) != 2:
            continue

        v = list(v)
        counter_pos.update(i for i, e in enumerate(v) if e[2]=='1')
        counter_total.update(range(len(v)))

    for k, v in counter_pos.most_common():
        print k, v, counter_total[k], 1. * v / counter_total[k]
