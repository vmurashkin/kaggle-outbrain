import sys
from itertools import groupby
from operator import itemgetter

if __name__ == '__main__':

    def iter_probs(lines):
        for l in sys.stdin:
            prob, v = l.split()
            display_id, ad_id = v.split(',', 2)[:2] 
            yield display_id, ad_id, float(prob)

    print "display_id,ad_id"
    for k, v in groupby(iter_probs(sys.stdin), key=itemgetter(0)):
        v = sorted(v, key=itemgetter(2), reverse=True)
        print '%s,%s' % (k, ' '.join(map(itemgetter(1), v)))
